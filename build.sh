#!/bin/bash
export PATH="/home/ubuntu/.nvm/versions/node/v18.16.0/bin/:$PATH"
git fetch 
git checkout main 
git reset --hard origin/main
npm run build
pm2 start "node build"
