import sveltePreprocess from "svelte-preprocess";
import adapter from '@sveltejs/adapter-node';

export default {
  kit: {
    adapter: adapter()
  },
  preprocess: [
    sveltePreprocess({
      typescript: false,
      scss: {
        prependData: '@use "src/variables.scss" as *;',
      },
    }),
  ],
  experimental: {
    useVitePreprocess: true,
  },
};
